FROM node:lts
ARG NODE_ENV
ENV NODE_ENV ${NODE_ENV}
ENV HOST 0.0.0.0
ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 8081
WORKDIR /app
COPY . .
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait
RUN npm install
RUN npm run build
EXPOSE 8081
CMD /wait && npm run start
