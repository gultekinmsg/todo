import {InteractionObject, Pact} from "@pact-foundation/pact";
import {pactWith} from "jest-pact";
import {getAll, create,} from '../../api/apiCalls';
import {like} from "@pact-foundation/pact/src/dsl/matchers";
import axios from "axios";
import {HTTPMethod} from "@pact-foundation/pact/src/common/request";

pactWith(
  {consumer: 'todo-ui', provider: 'todo-api', cors: true},
  (provider: Pact) => {
    describe('Get todo list', () => {
      beforeEach(() => {
        const interaction: InteractionObject = {
          state: 'There are todo items',
          uponReceiving: 'a get request to get todo list',
          withRequest: {
            method: HTTPMethod.GET,
            path: '/api/todo',
            headers: {Accept: 'application/json'}
          },
          willRespondWith: {
            status: 200,
            body: like(todoGenericListResponse),
            headers: getResponseHeaders()
          }
        };
        return provider.addInteraction(interaction);
      });
      it('returns todo list', async () => {
        axios.defaults.baseURL = provider.mockService.baseUrl;
        await getAll();
      });
    });
    describe('Create todo item', () => {
      beforeEach(() => {
        const interaction: InteractionObject = {
          state: 'Todo item created',
          uponReceiving: 'a post request to create todo item',
          withRequest: {
            method: HTTPMethod.POST,
            path: '/api/todo',
            headers: {'Content-Type': 'application/json'},
            body: like(createData)
          },
          willRespondWith: {
            status: 201,
            body: like(todoGenericItemResponse),
            headers: getResponseHeaders()
          }
        };
        return provider.addInteraction(interaction);
      });
      it('returns created todo item', async () => {
        axios.defaults.baseURL = provider.mockService.baseUrl;
        await create('test new todo item');
      });
    });
  });
function getResponseHeaders() {
  return {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  }
}

const todoGenericListResponse = {
  "items": [{"id": 1, "text": "task to do 1"}, {"id": 2, "text": "task to do 2"},
    {"id": 3, "text": "task to do 3"}, {"id": 4, "text": "task to do 4"},
    {"id": 5, "text": "task to do 5"}, {"id": 6, "text": "task to do 6"},
    {"id": 7, "text": "task to do 7"}, {"id": 8, "text": "task to do 8"},
    {"id": 9, "text": "task to do 0"}, {"id": 10, "text": "task to do 10"}],
  "message": "Success"
};
const todoGenericItemResponse = {
  "item": {"id": 11, "text": "task to do 11"},
  "message": "Success"
};
const createData = {"todo": "task to do"};
