import {render, screen} from "@testing-library/vue";
import "@testing-library/jest-dom";
import Item from '../../components/todo-item'

describe('Todo-item Unit Tests', () => {
  describe('Layout', () => {
    it('Has label', async () => {
      await render(Item, {props: {todo: todoItemData}});
      const label = await screen.findByTestId('todo-label1');
      expect(label).toBeInTheDocument();
    });
  });
});

const todoItemData = {"id": 1, "text": "task to do 1"};
