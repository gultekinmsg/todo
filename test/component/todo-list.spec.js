import {render, screen} from "@testing-library/vue";
import "@testing-library/jest-dom";
import List from '../../components/todo-list'

describe('Todo-list Unit Tests', () => {
  describe('Layout', () => {
    it('Has 5 label', async () => {
      await render(List, {props: {todos: todoListData}});
      await screen.findByTestId('todo-label4');
      const labels = await screen.findAllByTestId(/todo-label/);
      expect(labels.length).toBe(5);
    });
  });
});

const todoListData = [{"id": 1, "text": "task to do 1"}, {"id": 2, "text": "task to do 2"},
  {"id": 3, "text": "task to do 3"}, {"id": 4, "text": "task to do 4"}, {"id": 5, "text": "task to do 5"}]
