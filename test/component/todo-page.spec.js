import {render, screen} from "@testing-library/vue";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import App from '../../pages/index'
import {rest} from "msw";
import {setupServer} from "msw/node";
import Vuex from 'vuex'
import {createLocalVue} from "@vue/test-utils";
import {__createMocks as createStoreMocks} from '../../store/mocks';

let getCounter, postCounter = 0;
const server = setupServer(
  rest.get('/api/todo', (req, res, ctx) => {
    getCounter++;
    return res(ctx.status(200), ctx.json(todoListResponse));
  }),
  rest.post('/api/todo', (req, res, ctx) => {
    postCounter++;
    return res(ctx.status(200), ctx.json(todoCreateResponse));
  }),
);
beforeAll(() => server.listen());
beforeEach(() => {
  getCounter = 0;
  postCounter = 0;
  server.resetHandlers();
});
afterAll(() => server.close());
jest.mock('../../store')
const localVue = createLocalVue();
localVue.use(Vuex);
const setup = async () => {
  const storeMocks = createStoreMocks();
  await render(App, {store: storeMocks.store})
}

describe('Todo-App Unit Tests', () => {
  describe('Layout Tests', () => {
    describe('Todo Page', () => {
      it('Has todo app header', async () => {
        await setup();
        const header = screen.queryByRole('heading', {name: 'Todo App'});
        expect(header).toBeInTheDocument();
      });
      it('Has todo input', async () => {
        await setup();
        const input = screen.queryByPlaceholderText('Thing to-do');
        expect(input).toBeInTheDocument();
      });
      it('Has todo button', async () => {
        await setup();
        const button = screen.queryByRole("button", {name: 'Add'});
        expect(button).toBeInTheDocument();
      });
      it('Has 10 initial todo items label', async () => {
        await setup();
        await screen.findByTestId('todo-label9');
        const labels = await screen.findAllByTestId(/todo-label/);
        expect(labels.length).toBe(10);
      });
    });
    describe('Empty Data Page', () => {
      const emptyGet = rest.get('/api/list', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(todoListEmpty));
      });
      it('Has one initial data', async () => {
        server.use(emptyGet);
        await setup();
        const labels = await screen.findAllByTestId(/todo-label/);
        expect(labels.length).toBe(1);
      });
    });
  });
  describe('Interactions', () => {
    it('Adds new todo item to the list', async () => {
      await setup();
      const input = await screen.findByPlaceholderText('Thing to-do');
      await userEvent.type(input, 'task to do 11');
      const button = screen.queryByRole("button", {name: 'Add'});
      await userEvent.click(button);
      const newItem = await screen.findByText('task to do 11');
      expect(newItem).toBeInTheDocument();
    });
    it('Clears input value after adding new todo item to the list', async () => {
      await setup();
      const input = await screen.findByPlaceholderText('Thing to-do');
      await userEvent.type(input, 'task to do 11');
      const button = screen.queryByRole("button", {name: 'Add'});
      await userEvent.click(button);
      expect(input.textContent).toBe('');
    });

    it('It sends get request at the beginning of page', async () => {
      await setup();
      expect(getCounter).toBe(1);
    });
    it('It sends post request while adding new todo item to the list', async () => {
      await setup();
      const input = await screen.findByPlaceholderText('Thing to-do');
      await userEvent.type(input, 'task to do 11');
      const button = screen.queryByRole("button", {name: 'Add'});
      await userEvent.click(button);
      expect(postCounter).toBe(1);
    });
  });
});

const todoListResponse = {
  "items": [{"id": 1, "text": "task to do 1"}, {"id": 2, "text": "task to do 2"},
    {"id": 3, "text": "task to do 3"}, {"id": 4, "text": "task to do 4"},
    {"id": 5, "text": "task to do 5"}, {"id": 6, "text": "task to do 6"},
    {"id": 7, "text": "task to do 7"}, {"id": 8, "text": "task to do 8"},
    {"id": 9, "text": "task to do 0"}, {"id": 10, "text": "task to do 10"}],
  "message": "Success"
};
const todoCreateResponse = {"item": {"id": 11, "text": "task to do 11"}, "message": "Success"};
const todoListEmpty = {"items": null, "message": "Success"};
