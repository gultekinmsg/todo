import {create, getAll} from "@/api/apiCalls";

export const state = () => ({
  todos: [],
})

export const mutations = {
  getAll(state, todoArray) {
    if (todoArray != null) {
      state.todos = todoArray;
    }
  },
  create(state, todoItem) {
    state.todos.unshift(todoItem);
  }
}
export const actions = {
  async getAll({commit}) {
    const response = await getAll();
    commit('getAll', response.data.items);
  },
  async create({commit}, text) {
    const response = await create(text);
    commit('create', response.data.item);
  },
}
export const getters = {
  getAll(state) {
    return state.todos;
  }
};
