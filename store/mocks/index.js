import Vue from 'vue';
import Vuex from 'vuex';
import {create, getAll} from "@/api/apiCalls";

Vue.use(Vuex);

export const getters = {
  getAll(state) {
    return state.todos;
  }
};

export const mutations = {
  getAll(state, todoArray) {
    if (todoArray != null) {
      state.todos = todoArray;
    }
  },
  create(state, todoItem) {
    state.todos.unshift(todoItem);
  }
};

export const actions = {
  async getAll({commit}) {
    const response = await getAll();
    commit('getAll', response.data.items);
  },
  async create({commit}, text) {
    const response = await create(text);
    commit('create', response.data.item);
  },
};

export const state = {
  todos: [{
    id: 0,
    text: "Start using todo app",
  }],
};

export function __createMocks(custom = { getters: {}, mutations: {}, actions: {}, state: {} }) {
  const mockGetters = Object.assign({}, getters, custom.getters);
  const mockMutations = Object.assign({}, mutations, custom.mutations);
  const mockActions = Object.assign({}, actions, custom.actions);
  const mockState = Object.assign({}, state, custom.state);

  return {
    getters: mockGetters,
    mutations: mockMutations,
    actions: mockActions,
    state: mockState,
    store: new Vuex.Store({
      getters: mockGetters,
      mutations: mockMutations,
      actions: mockActions,
      state: mockState,
    }),
  };
}

export const store = __createMocks().store;
