
# Todo Frontend

You can store and retrieve your todos with this aplication.

To use/look frontend at staging or production:

**Staging url:** http://128.199.32.244:8081

**Production url:** http://206.189.14.14:8081

## Technologies and Tools Used

- Javascript as programming language
- Vue.js
- Nuxt
- Jest for testing
- Pact for contract testing
- Docker
- Docker-compose for deployment
- Gitlab CI for CI/CD tasks
- Wait for fixing docker-compose services start order (https://github.com/ufoscout/docker-compose-wait)

## Architecture
- We have two different servers, first one is `staging`, second one is `production`
- `staging` server is used for test purposes, acceptance tests are also running on this server.
- Inside each server, we have a docker-compose deployment.
- Compose file contains 3 different services.
  - `todoapp-api` Backend
  - `todoapp-web` Frontend
  - `todoapp-db` Postgresql database
- To deploy new version of application(api or web), ci/cd enters into server (staging and production)
  - Removes current containers(`docker-compose down`)
  - Pulls new image (`docker-compose pull`)
  - Starts containers (`docker-compose up -d`)
- see `.gitlab-ci.yml` for all stage and details of pipeline/

## Development
To run frontend for at local:
1. Enter to project's root directory and run backend and backend database with docker-compose :
> docker-compose up -d

**This will give you postgresql db and backend up and running.*
2. Then run frontend from terminal :
> npm install

> npm run dev

**Your frontend url will be localhost:8081*

## Deployment to Your Environment
You can create a docker-compose file and start your todo app with `docker-compose up -d` command

Sample docker-compose file;
```yml
version: '3.4'  
services:  
  todoapp-db:  
    image: postgres:14.1  
    environment:  
      - POSTGRES_DB=todoapp  
      - POSTGRES_USER=todousr  
      - POSTGRES_PASSWORD=strongpassword  
    volumes:  
      - postgres:/var/lib/postgresql/data  
    networks:  
      - todonet  
  todoapp-api:  
    image: msgmsg/todo-backend:stable  
    ports:  
      - 8080:8080  
    environment:  
      - "POSTGRES_URL=host=todoapp-db port=5432 user=todousr password=strongpassword dbname=todoapp sslmode=disable"  
  - "PORT=:8080"  
  - "WAIT_HOSTS=todoapp-db:5432"  
  networks:  
      - todonet  
  todoapp-web:  
    image: msgmsg/todo-frontend:production  
    ports:  
      - 8081:8081  
    environment:  
      - "WAIT_HOSTS=todoapp-api:8080"  
  networks:  
      - todonet  
volumes:  
  postgres:  
networks:  
  todonet:  
    driver: bridge
```
## Links
- Backend repository: https://gitlab.com/muhammedgultekin/todo-api
- Acceptance test repository: https://gitlab.com/muhammedgultekin/todo-acceptance

## Contributors
Muhammed Said Gültekin (gultekinmsg@gmail.com)
