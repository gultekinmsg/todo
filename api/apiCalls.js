import axios from "axios";

export const getAll = () => {
  return axios.get("/api/todo", {
    headers: {
      Accept: 'application/json',
    },
    baseURL: process.env.apiBaseURL,
  });
}

export const create = (text) => {
  return axios.post("/api/todo", {
    todo: text,
  }, {
    headers: {
      'Content-Type': 'application/json',
    },
    baseURL: process.env.apiBaseURL,
  });
}
